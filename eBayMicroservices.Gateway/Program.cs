using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using eBayMicroservices.Gateway.Infrastructure;
using Convey;
using Convey.Auth;
using Convey.Discovery.Consul;
using Convey.HTTP;
using Convey.LoadBalancing.Fabio;
using Convey.Logging;
using Convey.Security;
using Convey.Tracing.Jaeger;
using Convey.Types;
using Convey.WebApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Polly;


namespace eBayMicroservices.Gateway
{
    public class Program
    {
        public static Task Main(string[] args) => CreateHostBuilder(args).Build().RunAsync();
        private static ServiceProvider _serviceProvider;

        private static bool _isDevelopment;
        
        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            IHostBuilder hostBuilder = Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    bool isNullOrEmpty = string.IsNullOrWhiteSpace(hostingContext.HostingEnvironment.EnvironmentName);

                    _isDevelopment = hostingContext.HostingEnvironment.EnvironmentName == "Development";
                    
                    config
                        .SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
                        .AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", true,
                            true)
                        .AddJsonFile(isNullOrEmpty? "ocelot.json" : $"ocelot.{hostingContext.HostingEnvironment.EnvironmentName}.json",true,true)
                        .AddEnvironmentVariables();
                })
                .ConfigureWebHostDefaults(builder => builder
                        .ConfigureServices(services =>
                        {
                            string authenticationProviderKey = "OwnAuthProvider";

                            services.AddAuthentication()
                                .AddJwtBearer(authenticationProviderKey, x =>
                                {
                                    
                                });
                            services.AddSingleton<IPayloadBuilder, PayloadBuilder>();
                            services.AddSingleton<ICorrelationContextBuilder, CorrelationContextBuilder>();
                            services.AddSingleton<IAnonymousRouteValidator, AnonymousRouteValidator>();
                            services.AddTransient<IFabioClientProvider, FabioClientProvider>();
                            services.AddTransient<ResourceIdGeneratorMiddleware>();
                            services.AddOcelot()
                                .AddPolly()
                                .AddDelegatingHandler<CorrelationContextHandler>(true);

                            services
                                .AddConvey()
                                .AddErrorHandler<ExceptionToResponseMapper>()
                                .AddJaeger()
                                .AddHttpClient()
                                .AddConsul()
                                .AddFabio()
                                .AddJwt()
                                .AddSecurity()
                                .AddWebApi()
                                .Build();

                            ServiceProvider provider = services.BuildServiceProvider();
                            var configuration = provider.GetService<IConfiguration>();
                            services.Configure<AsyncRoutesOptions>(configuration.GetSection("AsyncRoutes"));
                            services.Configure<AnonymousRoutesOptions>(configuration.GetSection("AnonymousRoutes"));

                            _serviceProvider = services.BuildServiceProvider();
                        })
                        .Configure(app =>
                        {
                            app.UseConvey();
                            app.UseErrorHandler();
                            app.UseAccessTokenValidator();
                            app.UseAuthentication();
                            app.MapWhen(ctx => ctx.Request.Path == "/ping", a =>
                            {
                                a.Use((ctx, next) =>
                                {
                                    AppOptions appOptions = ctx.RequestServices.GetRequiredService<AppOptions>();
                                    return ctx.Response.WriteAsync($"Welcome to {appOptions.Name}");
                                });
                            });
                            app.MapWhen(ctx => ctx.Request.Path == "/docs", a =>
                            {
                                a.Use((ctx, next) =>
                                {
                                    StringBuilder sb = new StringBuilder();

                                    string protocolPrefix = _isDevelopment ? "http" : "https";
                                    
                                    foreach ((string name, string swaggerFile, string swagger) in ApiNameWithUrl)
                                    {


                                        sb.Append($"<a>{name}:</a><br/><br/>");
                                        sb.Append(
                                            $"<a href=\"{protocolPrefix}://{ctx.Request.Host}{swagger}\"> {name} Microservice Swagger</a><br/>");
                                        sb.Append(
                                            $"<a href=\"{protocolPrefix}://{ctx.Request.Host}{swaggerFile}\"> {name} Microservice Swagger File</a><br/><br/>");
                                    }
                                    
                                    return ctx.Response.WriteAsync(sb.ToString());
                                });
                            });
                            app.UseMiddleware<ResourceIdGeneratorMiddleware>();
                            app.UseOcelot(GetOcelotConfiguration()).GetAwaiter().GetResult();
                        })
                        .UseLogging()
                );


            return hostBuilder;
        }

        private static readonly IList<Tuple<string,string,string>> ApiNameWithUrl = new List<Tuple<string,string,string>>
        {
            new Tuple<string, string, string>("Identity","/api/identity/swagger/v1/swagger.json","/api/identity/swagger"),
            new Tuple<string, string, string>("Task Executor","/api/task-executor/swagger/v1/swagger.json","/api/task-executor/swagger"),
            new Tuple<string, string, string>("Feed","/api/feeds/swagger/v1/swagger.json","/api/feeds/swagger"),
            new Tuple<string, string, string>("Inventory", "/api/inventory/swagger/v1/swagger.json", "/api/inventory/swagger")
        };
        
        private static OcelotPipelineConfiguration GetOcelotConfiguration()
            => new OcelotPipelineConfiguration
            {
                AuthenticationMiddleware = 
                    async (context, next) 
                        => await AuthenticationMiddleware
                            .Invoke(context,next,_serviceProvider)
            };
    }
}