﻿namespace eBayMicroservices.Gateway.Models
{
    public class AuthenticationModel
    {
        public string Url { get; set; }
        public string JwtKey { get; set; }
    }
}