﻿using Convey.HTTP;

namespace eBayMicroservices.Gateway.Infrastructure
{
    public interface IFabioClientProvider
    {
        IHttpClient HttpClient();
    }
}