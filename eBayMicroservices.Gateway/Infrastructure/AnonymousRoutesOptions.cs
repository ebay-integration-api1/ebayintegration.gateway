using System.Collections.Generic;

namespace eBayMicroservices.Gateway.Infrastructure
{
    internal sealed class AnonymousRoutesOptions
    {
        public IEnumerable<string> Routes { get; set; }
    }
}