﻿using Convey.HTTP;

namespace eBayMicroservices.Gateway.Infrastructure
{
    public class FabioClientProvider : IFabioClientProvider
    {
        private readonly IHttpClient _httpClient;

        public FabioClientProvider(IHttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public IHttpClient HttpClient() => _httpClient;
    }
}