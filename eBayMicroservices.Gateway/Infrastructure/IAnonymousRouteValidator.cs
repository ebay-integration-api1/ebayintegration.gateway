namespace eBayMicroservices.Gateway.Infrastructure
{
    internal interface IAnonymousRouteValidator
    {
        bool HasAccess(string path);
    }
}