﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using eBayMicroservices.Gateway.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Ocelot.Middleware;
using Ocelot.Requester;

namespace eBayMicroservices.Gateway.Infrastructure
{
    public static class AuthenticationMiddleware
    {
        internal static async Task Invoke(DownstreamContext context, Func<Task> next,ServiceProvider serviceProvider)
        {
            if (!context.DownstreamReRoute.IsAuthenticated)
            {
                await next.Invoke();
                return;
            }

            if (context.HttpContext.RequestServices.GetRequiredService<IAnonymousRouteValidator>()
                .HasAccess(context.HttpContext.Request.Path))
            {
                await next.Invoke();
                return;
            }

            HttpResponseMessage authenticateResult = await InvokeIdentityService(context, serviceProvider);

            bool isOk = (authenticateResult.StatusCode == HttpStatusCode.OK);
            if (!isOk)
            {
                context.Errors.Add(new RequestCanceledError("Identity server is not available"));
                return;
            }

            Guid? userIdentity = await ReadResultContent(authenticateResult);

            if (userIdentity is null || userIdentity == Guid.Empty)
            {
                context.Errors.Add(new UnauthenticatedError("Unauthenticated"));
            }
            else
            {
                PrepareClaims(context, userIdentity);
                await next.Invoke();
            }
        }

        private static async Task<Guid?> ReadResultContent(HttpResponseMessage authenticateResult)
        {
            string str = await authenticateResult.Content.ReadAsStringAsync();

            Guid? jwtUserData = JsonConvert.DeserializeObject<Guid?>(str);
            return jwtUserData;
        }

        private static async Task<HttpResponseMessage> InvokeIdentityService(DownstreamContext context, ServiceProvider serviceProvider)
        {
            IFabioClientProvider fabioClientProvider = serviceProvider.GetService<IFabioClientProvider>();

            IConfiguration config = serviceProvider.GetService<IConfiguration>();

            AuthenticationModel authModel = config.GetSection("Authentication").Get<AuthenticationModel>();

            context.HttpContext.Request.Headers.TryGetValue(authModel.JwtKey, out StringValues values);

            HttpResponseMessage authenticateResult = await fabioClientProvider.HttpClient().PostAsync(
                authModel.Url, new
                {
                    Token = values.FirstOrDefault()
                });
            return authenticateResult;
        }

        private static void PrepareClaims(DownstreamContext context, Guid? jwtUserData)
        {
            context.DownstreamRequest.Headers.Authorization= AuthenticationHeaderValue.Parse(jwtUserData?.ToString());
        }
    }
}